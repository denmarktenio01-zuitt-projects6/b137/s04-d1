package b137.tenio.s04d1;

public class Main {
    public static void main(String[] args) {
        Car firstCar = new Car();
        firstCar.setName("Batmobile");
        firstCar.setBrand("Volkswagen");
        firstCar.setYearOfMake(1989);
        firstCar.drive();

        System.out.println(firstCar.getName());
        System.out.println(firstCar.getBrand());
        System.out.println(firstCar.getYearOfMake());

        Car secondCar = new Car("Avanza", "Toyota", 2005);
        secondCar.drive();


    }
}
